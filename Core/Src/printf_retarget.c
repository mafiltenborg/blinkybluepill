/*
 * printf_retarget.c
 *
 *  Created on: Jul 26, 2021
 *      Author: mfi
 */
#include "printf_retarget.h"	// Holds the UART_DEBUG define

int __io_putchar(uint8_t ch) /* This code overrides the right routine inside the belly of the stdio lib, making printf TX to UART as we'd expect it */
{
	HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);

#ifdef UART_DEBUG	/* Uses UART2 for debugging output */
	HAL_UART_Transmit(&huart2, &ch, 1U, 100U); // Yes, I know I should check for huart2 != NULL...
#else				/* Uses ITM for debugging output */
	ITM_SendChar(ch);	// Sends to ITM
#endif
	return ch;
}

//int _write(int file, char *ptr, int len)
//{
//  int i=0;
//  for( i=0; i<len; i++)
//	__io_putchar((*ptr++));
//  return len;
//}
