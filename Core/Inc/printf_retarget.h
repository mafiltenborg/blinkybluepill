#ifndef __PRINTF_RETARGET_H
#define __PRINTF_RETARGET_H

#define UART_DEBUG

#include <stdint.h>
#include "main.h"

int __io_putchar(uint8_t ch);

#endif /* __PRINTF_RETARGET_H */
